muttprint (0.73-12) unstable; urgency=medium

  * Converted d/copyright to machine readable format.
  * Trim trailing whitespace.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).
  * Switched from debhelper compat level 9 to non-parallell level 13.
  * Updated Standards-Version from 4.6.0 to 4.7.0.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 19 Oct 2024 10:24:30 +0200

muttprint (0.73-11) unstable; urgency=medium

  * migrate debian/rules to dh sequencer, keep custom make install
    (closes: #999327)

  [ Petter Reinholdtsen ]
  * Added myself as uploader.
  * Added upstream homepage link and metadata (Closes: #912178).
  * Added Vcs links to d/control.
  * Documented in d/control the build do not need root access.
  * Updated Standards-Version from 3.6.2 to 4.6.0.
  * Switched to debhelper compat level 13.
  * Removed trailing whitespace from d/changelog.

 -- Rene Engelhard <rene@debian.org>  Mon, 13 Dec 2021 19:41:55 +0000

muttprint (0.73-10) unstable; urgency=medium

  * readd imagemagick build-dep, we need it still for flipping

 -- Rene Engelhard <rene@debian.org>  Sat, 01 May 2021 17:45:51 +0200

muttprint (0.73-9) unstable; urgency=medium

  * stop using convert and include the pngs directly downloaded from
    sf.net svn to work around imagemagick now preventing basic convert
    usage (closes: #987552)

 -- Rene Engelhard <rene@debian.org>  Sat, 01 May 2021 15:41:32 +0200

muttprint (0.73-8.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 15:23:35 +0100

muttprint (0.73-8) unstable; urgency=medium

  * build-depend on automake, not automake1.11 (anymore) (closes: #865190)
  * don't aclocal/automake manually, use dh_autoreconf
  * bump dh compat to 9
  * migrate from dpatch to 3.0 (quilt) (closes: #668648)

 -- Rene Engelhard <rene@debian.org>  Mon, 19 Jun 2017 19:12:50 +0000

muttprint (0.73-7) unstable; urgency=medium

  * *really* fix Unescaped left brace in regex (closes: #814920)

 -- Rene Engelhard <rene@debian.org>  Fri, 22 Jul 2016 13:26:36 +0200

muttprint (0.73-6) unstable; urgency=medium

  * Raise debhelper compat level and build dependency to 5 (Closes: #817591)
  * fix Unescaped left brace in regex (closes: #814920)
  * add build-indep, use dh_prep

 -- Rene Engelhard <rene@debian.org>  Mon, 20 Jun 2016 07:35:24 +0200

muttprint (0.73-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch to automake1.11. (Closes: #724420)

 -- Eric Dorland <eric@debian.org>  Fri, 14 Mar 2014 23:30:08 -0400

muttprint (0.73-5) unstable; urgency=low

   * apply patch from Etienne Millon:
    + Fix "FTBFS: POD errors"
      Add a patch with encoding (Closes: #724200)
   * remove patch to run pod2man with --utf8 again

 -- Rene Engelhard <rene@debian.org>  Fri, 29 Nov 2013 21:30:49 +0100

muttprint (0.73-4) unstable; urgency=low

  * remove obsolete debconf note (closes: #631303, #658505)
  * run pod2man with --utf8 to fix german manpage encoding,
    thanks Helge Kreutzmann (closes: #631117)
  * update muttprint-manual.doc-base

 -- Rene Engelhard <rene@debian.org>  Fri, 10 Feb 2012 21:31:18 +0000

muttprint (0.73-3) unstable; urgency=low

  * 17_leave_tmp_dir.dpatch, also chdir() in the fatalError sub, thanks
    martin krafft again (closes: #551543)
  * suggest texlive-fonts-extra

 -- Rene Engelhard <rene@debian.org>  Wed, 15 Dec 2010 20:19:45 +0100

muttprint (0.73-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "cannot remove path when cwd is /tmp/muttprint*": new patch
    17_leave_tmp_dir.dpatch by martin krafft: change away from temp dir before
    removing it (closes: #551543).

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Nov 2010 19:31:49 +0100

muttprint (0.73-2) unstable; urgency=low

  * upload to unstable
  * add missing build-dep on docbook
  * add Russian debconf templates translation (closes: #537366)
  * move libtimedate-perl to recommends (closes: #419876)

 -- Rene Engelhard <rene@debian.org>  Fri, 14 Aug 2009 03:10:30 +0200

muttprint (0.73-1) experimental; urgency=low

  * new upstream release.
    - expands ~ in TO_FILE (closes: #480853)
    - fixes removal of e-mail sigs (closes: #431638)
  * remove tetex-* from Depends: (closes: #475009)
  * migrate from dbs to dpatch
  * add build-dep on autoconf/automake1.9 since releases now have no
    ./configure/Makefiles... pre-convert pics/ files to get pics/ build
    working
  * actually apply 03_fix_muttrc_changing_instructions and don't install
    README.pics anymore
  * debian/rules cleanup

 -- Rene Engelhard <rene@debian.org>  Mon, 29 Dec 2008 23:22:20 +0100

muttprint (0.72d-10) unstable; urgency=high

  * backport fix for 15_CVE-2008-5368 from upstrem (closes: #509487)
  * fix up lintian warnings

 -- Rene Engelhard <rene@debian.org>  Thu, 25 Dec 2008 22:32:04 +0100

muttprint (0.72d-9) unstable; urgency=low

  * add galician debconf translation, thanks Jacobo Tarrio (closes: #414037)

 -- Rene Engelhard <rene@debian.org>  Mon, 12 Mar 2007 09:06:18 +0100

muttprint (0.72d-8) unstable; urgency=low

  * remove muttprint/utf8 debconf question, depend on perl (>= 5.8.0-16)
    instead (closes: #388940)

 -- Rene Engelhard <rene@debian.org>  Sun, 24 Sep 2006 19:44:30 +0200

muttprint (0.72d-7) unstable; urgency=low

  * change depends to latex-ucs | texlive-latex-recommended, thanks
    Magnus Therning (closes: #388201)

 -- Rene Engelhard <rene@debian.org>  Tue, 19 Sep 2006 11:08:11 +0200

muttprint (0.72d-6) unstable; urgency=low

  * Depend on tetex-ucs now since "Unicode locales *are* default [...] for
    etch"... (closes: #262485)
  * make Build-Depends-Indep Build-Depends:
  * add pt debconf translation, thanks Rui Branco (closes: #381690)
  * add updated sv debconf translation, thanks Daniel Nylander (closes: #350631)

 -- Rene Engelhard <rene@debian.org>  Mon, 11 Sep 2006 19:17:22 +0200

muttprint (0.72d-5) unstable; urgency=low

  * support texlive-* in muttprints Depends

 -- Rene Engelhard <rene@debian.org>  Mon, 23 Jan 2006 05:44:20 +0100

muttprint (0.72d-4) unstable; urgency=low

  * readd apparently lost patch to point to /usr/share/ospics for the images
    instead of /usr/share/muttprint, thanks David Moreno Garza
  * add sv po-debconf translation, thanks Daniel Nylander (closes: #340970)

 -- Rene Engelhard <rene@debian.org>  Sun, 22 Jan 2006 22:20:05 +0100

muttprint (0.72d-3) unstable; urgency=low

  * readd uncorrupted Debian.png from CVS (closes: #342277)

 -- Rene Engelhard <rene@debian.org>  Tue,  3 Jan 2006 10:51:23 +0100

muttprint (0.72d-2) unstable; urgency=low

  * lower slrn Recommends to Suggests. Remove versioned Recommends: on
    perl which was only "necessary" for woody. Same with the versioned
    tetex-extra Suggests (closes: #333608)
  * fix typo in de.po, thanks Jens Seidel (closes: #313964)
  * add vi.po, thanks Clytie Siddall (closes: #312993)
  * s/apperently/apparently/, thanks Clytie Siddall (closes: #312994)
  * fix bashism in config script
  * Standards-Version: 3.6.2 (no changes needed)

 -- Rene Engelhard <rene@debian.org>  Thu, 13 Oct 2005 16:34:20 +0200

muttprint (0.72d-1) unstable; urgency=high

  * New micro upstream release
    - only upstream maintainer change, spelling fixes
    - disables MAINT_SEARCH (closes: #309892)
  * add ja debconf translation, thanks Hideki Yamane (closes: #290143)

 -- Rene Engelhard <rene@debian.org>  Fri, 20 May 2005 17:12:44 +0200

muttprint (0.72c-2) unstable; urgency=low

  * add cs debconf translation, thanks Miroslav Kure (closes: #273710)
  * add it debconf translation, thanks Luca Monducci (closes: #277840)
  * don't mention we have HTML docs, we don't
  * don't install sample-* into example/ anymore, therefore no symlinks
    anymore (closes: #266655)

 -- Rene Engelhard <rene@debian.org>  Sat,  1 Jan 2005 03:00:55 +0100

muttprint (0.72c-1) unstable; urgency=low

  * New upstream release.
  * update pt_BR debconf translation, thanks Andre Luis Lopes
    (closes: #264224)

 -- Rene Engelhard <rene@debian.org>  Sun, 26 Dec 2004 23:24:24 +0100

muttprint (0.72a-3) unstable; urgency=low

  * add Build-Depends: on sharutils (closes: #249737)

 -- Rene Engelhard <rene@debian.org>  Wed, 19 May 2004 09:37:00 +0200

muttprint (0.72a-2) unstable; urgency=low

  * readd the .png versions of the images (except the b/w penguin) back
    from upstream CVS, same with penguin.jpg; readd flipped BabyTux for png
    since there imagemagick actually seems to work....

 -- Rene Engelhard <rene@debian.org>  Sun, 16 May 2004 00:39:03 +0200

muttprint (0.72a-1) unstable; urgency=low

  * New upstream release.
  * add updated fr.po, thanks Christian Perrier (closes: #245759)
  * remove the .png, .jpg and flipped versions for now since
    neither imagemagick nor gimp with script-fu work now on thwm *sigh*

 -- Rene Engelhard <rene@debian.org>  Sun, 25 Apr 2004 13:15:45 +0200

muttprint (0.72-3) unstable; urgency=low

  * remove the HTML stuff from doc-base and add the PDF stuff
    (closes: #243073, #243160)

 -- Rene Engelhard <rene@debian.org>  Mon, 12 Apr 2004 16:11:11 +0200

muttprint (0.72-2) unstable; urgency=low

  * The "Ugh-now-I-remember why gs was in Build-Depends:" release.

  * readd gs to Build-Depends: (closes: #243134)

 -- Rene Engelhard <rene@debian.org>  Sun, 11 Apr 2004 14:08:24 +0200

muttprint (0.72-1) unstable; urgency=low

  * The "people interested in muttprint should read
    http://muttprint.sourceforge.net/future.shtml and probably become new
    upstream" release.

  * new upstream release
    - does not choke on minimalistic mails anymore (closes: #241912)
  * New patch to fix building the documentation; use -d to the db2* tools
    disable html generation stuff though since that somehow doesn't work
    -> only PDF for now
  * add Build-Depends: on docbook-utils and clean up the rest
  * Recommend perl (>= 5.8.1)
  * Replace muttprint (<< 0.70-1) instead of Conflicts: (closes: #214957)
  * converted changelog to UTF8
  * merge 0.71-7

 -- Rene Engelhard <rene@debian.org>  Fri,  9 Apr 2004 22:01:05 +0200

muttprint (0.71+0.72pre6-1) experimental; urgency=low

  * CVS co (HEAD / 0.72pre6)
    - handles utf8 right (closes: #200682, #200864)
  * Depends: libtext-iconv-perl
  * add dialog and perl (>= 5.8.0-16) to Suggests:
  * temporarily back out 03_fix_muttrc_changing_instructions
  * add debconf warning for people trying to install muttprint on systems
    having perl (<< 5.8.0-16) and the possibility to have utf8 used is there
  * Standards-Version: 3.6.1
  * enhance /etc/Muttprintrc with all possible variables; set to default
  * remove CHARSET variable since it does not have an effect anymore...

 -- Rene Engelhard <rene@debian.org>  Fri, 17 Oct 2003 16:12:34 +0200

muttprint (0.71-7) unstable; urgency=low

  * add catalan debconf template translation;
    thanks Aleix Badia i Bosch (closes: #236654)

 -- Rene Engelhard <rene@debian.org>  Sat, 13 Mar 2004 22:46:27 +0100

muttprint (0.71-6) unstable; urgency=low

  * add dutch debconf template translation - thanks
    Tim Vandermeersch (closes: #209104)
  * do not use | in Enhances: (closes: #204278)

 -- Rene Engelhard <rene@debian.org>  Tue,  7 Oct 2003 20:09:50 +0200

muttprint (0.71-5) unstable; urgency=low

  * add spanish debconf template translation - thanks
    Carlos Alberto Martín Edo (closes: #201138)
  * bump Standards-Version: to 3.6.0

 -- Rene Engelhard <rene@debian.org>  Mon, 14 Jul 2003 18:56:22 +0200

muttprint (0.71-4) unstable; urgency=low

  * add brazlian potugese debconf template translation - thanks
    Andre Luis Lopez (closes: #197517)

 -- Rene Engelhard <rene@debian.org>  Sun, 15 Jun 2003 20:20:06 +0200

muttprint (0.71-3) unstable; urgency=low

  * add french debconf template translation - thanks Christian Perrier
    (closes: #195649)

 -- Rene Engelhard <rene@debian.org>  Sun,  8 Jun 2003 12:52:52 +0200

muttprint (0.71-2) unstable; urgency=low

  * add latex-ucs to Suggests: as it is now in Debian
  * move psutils to Suggests:
    (and add both package to Muttprintrc's comments..)

 -- Rene Engelhard <rene@debian.org>  Fri,  6 Jun 2003 18:43:10 +0200

muttprint (0.71-1) unstable; urgency=low

  *  new upstream release.
  *  The compface feature actually needs imagemagick too, so
     add imagemagick to Suggests: and the comment in /etc/Muttprintrc

 -- Rene Engelhard <rene@debian.org>  Tue, 27 May 2003 22:02:40 +0200

muttprint (0.70-2) unstable; urgency=low

  * fix muttprint's and muttprint-manual's description (closes: #191965)

 -- Rene Engelhard <rene@debian.org>  Mon,  5 May 2003 19:16:37 +0200

muttprint (0.70-1) unstable; urgency=low

  * new upstream release.
  * ospics: since the new upstream tarball does not contain the *.png and
    *.jpg images anymore, create them during build with convert; make
    all images be available in all three formats
  * removed unneeded Conflicts: in ospics package
  * as the muttprint package itself now becomes bigger and bigger because
    of the needed space of the manual, spit the manual out to
    muttprint-manual, this reduces the main package of around 390 KB..

 -- Rene Engelhard <rene@debian.org>  Sun, 27 Apr 2003 20:04:56 +0200

muttprint (0.64+0.70pre1-2) unstable; urgency=low

  * kill CVS dirs from debian/ *sigh*

 -- Rene Engelhard <rene@debian.org>  Sun, 20 Apr 2003 03:08:36 +0200

muttprint (0.64+0.70pre1-1) unstable; urgency=low

  * new upstream (pre-)version (CVS HEAD 2003-04-19)
    - does not create /tmp/muttprint-geometry-ver anymore (closes: #180615)
    - handles CHARSET="auto" (closes: #154554)
      - CHARSET="auto" as default in /etc/Muttprintrc
    - "unitialized value" perl warning fixed (closes: #175182)
    - removed patch 09_translations_in_usr_share and the appropriate
      snippet in rules as the files are moved to /usr/share upstream now.
    - new: koi8-r support
    - new: experimental utf8 support; do not write in the explanatory
      comments in /etc/Muttprintrc until the needed latex-ucs is in Debian
  * added suggestions:
    - compface -  needed for using the X-Face printing feature
    - tetex-extra (>= 2.0-1) - koi8-r support needs tetex 2.x but
      muttprint itself runs fine with older versions, so only suggest
  * move mutt from Suggests: to Recommends: and recommend not only mutt;
    recommend MUA's we know they (can) ouput plain text and mail-reader:
    mutt | sylpheed | gnus | xfmail | exmh | mail-reader; do the same
    with news-readers: slrn | news-reader
  * Enhances: mutt, slrn im muttprint; Enhances: muttprint in ospics
  * remove "of Mutt" from muttprint's short description as muttprint
    can be used with many MUAs
  * some formulation beautification/fixes on the "pictures moved" debconf
    note
  * write near the options in /etc/Muttprintrc which additional package
    is needed (for the suggested packages)
  * converted to po-debconf; Build-Depend: on debhelper (>= 4.1.16)
  * Standards-Version: 3.5.9
  * debhelper compatibility level 4; use ${misc:Depends}

 -- Rene Engelhard <rene@debian.org>  Sat, 19 Apr 2003 19:08:47 +0200

muttprint (0.64-3) unstable; urgency=low

  * remove cruft from source package *sigh*

 -- Rene Engelhard <rene@debian.org>  Tue, 25 Mar 2003 22:24:01 +0100

muttprint (0.64-2) unstable; urgency=low

  * fix checking if we install new or upgrade from <= 0.63a in config
    (closes: #174623)

 -- Rene Engelhard <rene@debian.org>  Sun, 29 Dec 2002 16:14:30 +0100

muttprint (0.64-1) unstable; urgency=low

  * new upstream version
    - removed compat2 patch because upstream added a handler routine
      determining which geometry.sty we're using and adding compat2 if
      necessary or not

 -- Rene Engelhard <rene@debian.org>  Sun, 15 Dec 2002 21:22:27 +0100

muttprint (0.63a-6) unstable; urgency=low

  * geometry.sty changed heavily in the new tetex packages which
    made printing the footer on each page not visible anymore... :(
    Therefore added compat2 to the options of the \usepackage for
    geometry.sty in /usr/bin/muttprint (closes: #172715)
  * /etc/Muttprintrc: turn on rule before/after header/footer by default
  * split the images out to extra 'ospics' package to let people install
    them without installing tetex-extra
    - added debconf note for people upgrading from an older version that
      they have to install ospics additionally and change /usr/share/muttprint
      to /usr/share/ospics
  * fixed some lintian/linda warnings
  * bumped Standards-Version: to 3.5.8 (no changes needed)

 -- Rene Engelhard <rene@debian.org>  Thu, 12 Dec 2002 23:47:52 +0100

muttprint (0.63a-5) unstable; urgency=low

  * Description: s/XFMail and PINE//, s/Netscape Messenger/Mozilla/
    and restructured the lines (closes: #170658)
  * do not specify PAPER; let muttprint get that from /etc/papersize
    (closes: #170821)

 -- Rene Engelhard <rene@debian.org>  Sat, 30 Nov 2002 12:00:50 +0100

muttprint (0.63a-4) unstable; urgency=low

  * added jadetex and gs to Build-Depends: (closes: #164979)

 -- Rene Engelhard <rene@debian.org>  Wed, 16 Oct 2002 12:27:16 +0200

muttprint (0.63a-3) unstable; urgency=low

  * do not install upstream changelog twice...

 -- Rene Engelhard <rene@debian.org>  Sun, 22 Sep 2002 09:18:28 +0200

muttprint (0.63a-2) unstable; urgency=low

  * Hrmpf, the current default settings do not work on all systems
    (ppl have to modify /etc/Muttprintrc first)
    So:
     - disable any definition of PRINTER= in /etc/Muttprintrc
     - set PRINT_COMMAND in /etc/muttprintc to just "lpr"
   Thhis have the effect that the default printer of the current
   print system is used; if anyone wants to configure anything other
   he has to do it him-/herself.... The whole fixes closes: #161329)

 -- Rene Engelhard <rene@debian.org>  Wed, 18 Sep 2002 20:19:03 +0200

muttprint (0.63a-1) unstable; urgency=low

  * new upstream release.
    - now using FONT="Latex" instead of FONT="Latex-bright" per default
      in /etc/Muttprintrc -- Latex-bright isn't available in Debian
  * removed patches that were introduced in 0.63-1 and -2 and became
    obsolete by the new upstream release:
     - 02_fix_html_generation
     - 04_fix_paths_in_documentation
     - 05_remove_shebang
     - 06_remove_use_cwd
     - 07_fix_duplex_printer
  * debian/rules: changend the build; we now have to do a make clean in
    doc/manpages and doc/manual and then to a make docdir=... sharedir=...
    and so on in these directories

 -- Rene Engelhard <rene@debian.org>  Thu, 12 Sep 2002 03:59:30 +0200

muttprint (0.63-2) unstable; urgency=low

  * added patch from upstream to get DUPLEX="printer" to work again
    Thanks, Bernhard (closes: #160312)

 -- Rene Engelhard <rene@debian.org>  Tue, 10 Sep 2002 19:47:32 +0200

muttprint (0.63-1) unstable; urgency=low

  * new upstream release
  * moving to use dbs
  * going back to only create html & pdf
  * also provide "flipped" penguin (BabyTuX; the "normal" one doesn't
    make sense) images, which has the effect that the penguin is looking
    _to_ the mail, add imagemagick to Build-Depends-Indep: for that
    (closes: #151135)
  * fixed a lintian warning (remove #!/usr/bin/perl from translation-de.pl
    as in the other translation-$(lang).pl files; these files are only for
    using for inclusion from muttprint; not as scripts which should executed
    standalone)
  * add symlinks: /usr/share/doc/muttprint/sample-muttprintrc-$(lang)
    to /usr/share/doc/muttprint/examples/sample-muttprintrc-$(lang).
    This makes this path more like RedHat's one and makes it easier to ...
  * ... change manual to point to the right location of documentation
    We only have to point to two different locations, not three...
    This change is only done for _de_ and _en_, I do not speak the other
    languages (es/fr/it) good, so help there (send me patches) would be
    appreciated... The changes for de and en are in patch
    04_fix_paths_in_documentation.
    So this only partially closes: #159480
  * fixed "Subroutine main::getcwd redefined" warning
    Thanks to Andreas Klapschus <klappi@lottinet.ping.de> for the patch
    (closes: #159774).
  * bumped Standards-Version: to 3.5.7

 -- Rene Engelhard <rene@debian.org>  Mon,  9 Sep 2002 07:34:07 +0200

muttprint (0.62b-3) unstable; urgency=low

  * after finding out how to fix the build failure at html generation,
    reenable its generation and installation (indirectly closes: #137520)

 -- Rene Engelhard <rene@debian.org>  Fri, 30 Aug 2002 20:53:02 +0200

muttprint (0.62b-2) unstable; urgency=low

  * new maintainer (closes: #158475)
  * fix error in manual (closes: #138205)
  * fix typo in doc-base (closes: #137520)
  * package now builds (closes: #142161)
    - disabling html doc generation
    - enabling generation of dvi and txt additional to the left pdf
    - fixing Makefiles for manuals to only depend on default not on all
    - do not copy manual-$(lang)/*, copy manual-$(lang)*
  * removed double listing of /etc/Muttprintrc as conffile; one time is
    enough :)
  * do not install the pdf version of the manual twice
  * add Suggests: to libtimedate-perl (closes: #145913)

 -- Rene Engelhard <rene@debian.org>  Wed, 28 Aug 2002 04:00:23 +0200

muttprint (0.62b-1) unstable; urgency=low

  * New upstream release.

 -- Chanop Silpa-Anan <chanop@debian.org>  Mon, 25 Feb 2002 14:48:31 +1100

muttprint (0.61-3) unstable; urgency=low

  * Fix Build-Depends-Indep. (closes: #133720)

 -- Chanop Silpa-Anan <chanop@debian.org>  Wed, 13 Feb 2002 23:34:22 +1100

muttprint (0.61-2) unstable; urgency=low

  * Fix docbase.

 -- Chanop Silpa-Anan <chanop@debian.org>  Wed, 13 Feb 2002 08:50:01 +1100

muttprint (0.61-1) unstable; urgency=low

  * New upsteam release.
  * Fix long package description. (closes: #131014)

 -- Chanop Silpa-Anan <chanop@debian.org>  Wed, 13 Feb 2002 04:32:52 +1100

muttprint (0.60-2) unstable; urgency=low

  * Fix typos. (closes: #121539, #121535)
  * Honour PRINTER environment. (closes: #121537)

 -- Chanop Silpa-Anan <chanop@debian.org>  Thu, 29 Nov 2001 11:31:03 +1100

muttprint (0.60-1) unstable; urgency=low

  * New upstream release. (closes: #111233, #111235, #113760)

 -- Chanop Silpa-Anan <chanop@debian.org>  Sun, 25 Nov 2001 22:53:45 +1100

muttprint (0.53a-1) unstable; urgency=low

  * New upstream release. (closes: #108063)
  * The patch I sent to upsteam to close Bug#108063 only deals with
    a4 and letter in /etc/papersize since the code in muttprint only
    knows A4 and letter. Default fallback is A4 though.

 -- Chanop Silpa-Anan <chanop@debian.org>  Mon, 13 Aug 2001 00:47:11 +1000

muttprint (0.52a-1) unstable; urgency=low

  * New upstream release.
  * Use Standards-Version: 3.5.6 (woody ready)

 -- Chanop Silpa-Anan <chanop@debian.org>  Thu,  2 Aug 2001 09:42:06 +1000

muttprint (0.51-1) unstable; urgency=low

  * New upstream release.
  * Now support hardware duplexing with postscript printer.

 -- Chanop Silpa-Anan <chanop@debian.org>  Wed, 11 Jul 2001 16:22:02 +1000

muttprint (0.50b-1) unstable; urgency=low

  * New upstream release.
  * Remove procmail from dependency since upstream does not use it anymore.
  * There are documents on how people should set muttrc (unignore date)
    in the package from upstream. (closes: #91974)

 -- Chanop Silpa-Anan <chanop@debian.org>  Thu,  5 Jul 2001 08:11:03 +1000

muttprint (0.50a-1) unstable; urgency=low

  * New upstream release.
  * Fix one typo bug I found in 0.50.

 -- Chanop Silpa-Anan <chanop@debian.org>  Tue,  3 Jul 2001 20:04:45 +1000

muttprint (0.50-1) unstable; urgency=low

  * New upstream release.
  * Update debian/rules.
  * Add dependency for perl. Since this version is rewritten in perl.
  * Default the printer to "lp"

 -- Chanop Silpa-Anan <chanop@debian.org>  Tue,  3 Jul 2001 16:40:26 +1000

muttprint (0.41-3) unstable; urgency=low

  * New maintainer; closes: #100246
  * Standards-Version 3.5.5.

 -- Chanop Silpa-Anan <chanop@debian.org>  Fri, 15 Jun 2001 19:42:19 +1000

muttprint (0.41-2) unstable; urgency=low

  * Fixed an error in README; closes: #97588

 -- Dr. Guenter Bechly <gbechly@debian.org>  Tue, 15 May 2001 19:38:43 +0200

muttprint (0.41-1) unstable; urgency=low

  * New upstream release.
  * Updated Debian package internals.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sat, 12 May 2001 15:10:47 +0200

muttprint (0.40-3) unstable; urgency=low

  * Added dependency on procmail; closes: #94582

 -- Dr. Guenter Bechly <gbechly@debian.org>  Fri, 20 Apr 2001 16:52:23 +0200

muttprint (0.40-2) unstable; urgency=low

  * Changed the default paper format from letter to A4 because A4 is
    more widely used all over the world (except USA and Canada).

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sun, 15 Apr 2001 15:05:36 +0200

muttprint (0.40-1) unstable; urgency=low

  * New upstream release.
  * Changed defaults in sample-muttprintrc.
  * Deleted Debian's own manpage, because it was incorporated by upstream into the
    new release.
  * Updated to latest standards version (changed control and rules).

 -- Dr. Guenter Bechly <gbechly@debian.org>  Wed, 11 Apr 2001 19:30:28 +0200

muttprint (0.30-2) unstable; urgency=low

  * Removed README.Debian and modified manpage.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Wed, 28 Mar 2001 11:54:09 +0200

muttprint (0.30-1) unstable; urgency=low

  * New upstream release; closes: #89820
  * Corrected typo in README.Debian; closes: #89347
  * Corrected typo in copyright file.
  * Adjusted sample-muttprintrc and Makefile for this new release.
  * Adjusted the dependencies in control for this new release.
  * Adjusted the manpage for this new release, and use of CUPS.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Tue, 27 Mar 2001 10:48:07 +0200

muttprint (0.23-2) unstable; urgency=low

  * Corrected typo in description; closes: #88404.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sun,  4 Mar 2001 14:55:54 +0100

muttprint (0.23-1) unstable; urgency=low

  * Initial Release; closes: #88218
  * Added a Makefile.
  * Wrote a manpage.
  * Changed defaults in sample-muttprintrc.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Tue, 27 Feb 2001 20:22:41 +0100
